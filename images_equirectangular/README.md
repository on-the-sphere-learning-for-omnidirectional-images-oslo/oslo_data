# Copyright
1- image "01.JPG" is copyrighted by Navid Mahmoudian Bidgoli and is captured by a RICOH THETA Z1 device.

2- image "02.jpg" is entitled "Oberkaisern" and is attributed to "Ansgar Koreng / CC BY-SA 4.0".

3- image "03.jpeg" is entitled "360 panorama shore" and is taken from Pixexid.

4- image "04.jpeg" is entitled "360 panorama view kitchen" and is taken from Pixexid.

5- image "05.jpeg" is entitled "360 image marina" and is taken from Pixexid.

6- image "06.jpeg" is entitled "Times Square 360" and is taken from Pixexid.

7- image "07.jpeg" is entitled "cadillac escalade 360 interior" and is taken from Pixexid.

8- image "08.jpeg" is entitled "360 panorama view house interior" and is taken from Pixexid.

9- image "09.jpeg" is entitled "360 image of a room" and is taken from Pixexid.

10- image "10.jpeg" is entitled "A large living room with a large glass wall" and is taken from Pixexid.
